/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapinheritance;

/**
 *
 * @author User
 */
public class TastShape {

    public static void main(String[] args) {
        circle circle = new circle(3);
        circle.print();

        Triangle triangle = new Triangle(3, 5);
        triangle.print();
        
        Rectangle rectangle = new Rectangle(4,5);
        rectangle.print();
        
        Square square = new Square(2);
        square.print();
        
        shape[] shapes={circle,triangle,rectangle,square};
        for(int i=0; i<shapes.length; i++){
            System.out.println(shapes[i].calArea());
        }
        
  
    }         
}