/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapinheritance;

/**
 *
 * @author User
 */
public class Triangle extends shape{

    protected double base;
    protected double height;
    static final double pi = 0.5;

    public Triangle(double base, double height) {
        //super();
        System.out.println("Triangle Create");
        this.base = base;
        this.height = height;
        
    }

    @Override
    public double calArea() {
        return pi * base * height;
    }

    
    public void print() {
        System.out.println("TriangleArea = " + this.calArea());
    }
}
