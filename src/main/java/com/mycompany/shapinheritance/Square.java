/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapinheritance;

/**
 *
 * @author User
 */
public class Square extends Rectangle {

    private double side;

    public Square(double side) {
        super(side,side);
        System.out.println("Square Created");
        this.side = side;
    }

    @Override
    public double calArea() {
        return side * side;

    }
    public void print() {
        System.out.println("SquareArea = " +this.calArea());
    }
}
