/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapinheritance;

/**
 *
 * @author User
 */
public class Rectangle extends shape{

    private double width;
    private double height;

    public Rectangle(double width, double height) {
        //super();
        System.out.println("Rectangle Create");
        this.height = height;
        this.width = width;
    }

    @Override
    public double calArea() {
        return width * height;
    }
    
    public void print() {
        System.out.println("RectangleArea = " + this.calArea());
    }
}
